;; Nope No Ugly Bar
(setq linum-relative-current-symbol "")
(setq ring-bell-function 'ignore)
(setq backup-directory-alist `(("." . "~/.cache/emacs/autosaves")))
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(fido-vertical-mode)

(add-to-list 'load-path "~/.config/emacs/custom")

;; Simp C-Mode
(require 'simpc-mode)
(add-to-list 'auto-mode-alist '("\\.[hc]\\(pp\\)?\\'" . simpc-mode))

;; Odin Mode
(require 'odin-mode)
(add-to-list 'auto-mode-alist '("\\.odin\\'" . odin-mode))

;; Fasm Mode
(require 'fasm-mode)
(add-to-list 'auto-mode-alist '("\\.asm\\'" . odin-mode))

;; Change Font
(set-frame-font "JetBrains Mono 13")
(add-to-list 'default-frame-alist '(font . "JetBrains Mono 13" ))

;; Add Repos
(require 'package)
	(add-to-list 'package-archives '("gnu"   . "https://elpa.gnu.org/packages/") t)
	(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; No Littering
(use-package no-littering
  :ensure t
  :init
  (setq custom-file (no-littering-expand-etc-file-name "custom.el"))
  (setq auto-save-file-name-transforms
		`((".*" ,(no-littering-expand-var-file-name "auto-save/") t))))

(use-package hl-todo
  :ensure t
  :config
  (setq hl-todo-keyword-faces
		'(("TODO"   .	"#FF0000")
          ("FIXME"  .	"#FF0000")
          ("HACK"	.	"#FF4500")
          ("NOTE"   .	"#1E90FF")))
  :init
  (global-hl-todo-mode))

;; Magit
(use-package magit
  :ensure t)

;; Git Gutter
(use-package git-gutter
  :ensure t
  :config
  (global-git-gutter-mode +1))

;; Nerd Icons Dired
(use-package nerd-icons-dired
  :ensure t
  :hook
  (dired-mode . nerd-icons-dired-mode))

;; Smart Parens
(use-package smartparens-mode
  :ensure smartparens
  :hook (prog-mode text-mode markdown-mode)
  :config
  (require 'smartparens-config))

;; ORG
(use-package org
  :pin gnu
  :ensure t
  :hook
  (org-mode . visual-line-mode)
  :config
  ;; inside .emacs file
  (setq org-latex-listings 'minted
        org-latex-packages-alist '(("outputdir=./build" "minted" nil)))
  (setq org-latex-pdf-process (list "latexmk -xelatex -shell-escape -bibtex -f %f"))
  (setq org-latex-minted-options '(("breaklines" "trube")
                                   ("breakanywhere" "true"))))

;; ORG ROAM
(use-package org-roam
  :ensure t)

;; ORG ROAM UI
(use-package org-roam-ui
  :ensure t
  :after org-roam
  ;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
  ;;         a hookable mode anymore, you're advised to pick something yourself
  ;;         if you don't care about startup time, use
  ;;  :hook (after-init . org-roam-ui-mode)
  :config
  (setq org-roam-ui-sync-theme t
        org-roam-ui-follow t
        org-roam-ui-update-on-save t
        org-roam-ui-open-on-start t))

;; WEBSOCKET
(use-package websocket
  :ensure t
  :after org-roam)

;; PDF Tools
(use-package pdf-tools
  :ensure t
  :mode ("\\.pdf\\'" . pdf-view-mode)
  :config
  (pdf-tools-install)
  :custom
  (pdf-view-display-size 'fit-page)
  (pdf-annot-activate-created-annotations t))

;; Corfu
(use-package corfu
  :ensure t
  :custom
  (corfu-cycle t)
  (corfu-auto t)
  (corfu-quit-at-no-match t)
  :init
  (global-corfu-mode))

;; Nerd Icons Corfu
(use-package nerd-icons-corfu
  :ensure t
  :config
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

(use-package writeroom-mode
  :ensure t
  :hook
  (writeroom-mode . my/disable-line-numbers-in-writeroom)
  (writeroom-mode-disable . my/enable-line-numbers-after-writeroom)
  :config
  (setq writeroom-width 96)
  (defun my/disable-line-numbers-in-writeroom ()
    (display-line-numbers-mode -1))
  (defun my/enable-line-numbers-after-writeroom ()
      (display-line-numbers-mode 1))
  (defun my/writeroom-word-count ()
    "Return the word count as a string."
    (format "Words: %d" (count-words (point-min) (point-max))))
  (setq writeroom-mode-line
        '((:eval (my/writeroom-word-count))
          " "
          writeroom-mode-line-misc-info)))

(use-package adaptive-wrap
  :ensure t
  :hook (writeroom-mode . adaptive-wrap-prefix-mode))

;; Eglot
(use-package eglot
  :ensure t
  :hook
  (tex-mode . eglot-ensure))

;; Cape
(use-package cape
  :ensure t
  :init
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-elisp-block)
  (add-to-list 'completion-at-point-functions #'cape-tex)
  (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster))

;; Bind Key
(use-package bind-key
  :ensure t)

;; Gruber Darker
(use-package gruber-darker-theme
  :ensure t)

;; Emacs Config
(use-package emacs
  :init
  (load-theme 'gruber-darker t)
  (ido-mode 1)
  :config
  (with-eval-after-load 'ox-latex
	(add-to-list 'org-latex-classes
				 '("org-plain-latex"
				   "\\documentclass{article}
           [NO-DEFAULT-PACKAGES]
           [PACKAGES]
           [EXTRA]"
				   ("\\section{%s}" . "\\section*{%s}")
				   ("\\subsection{%s}" . "\\subsection*{%s}")
				   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
				   ("\\paragraph{%s}" . "\\paragraph*{%s}")
				   ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))
  (setq org-latex-default-class "org-latex-pl")
  (bind-keys*
   ("C-c C-k" . compile)
   ("C-c k" . kill-compilation)
   ("C-," . duplicate-line))
  :hook
  ((prog-mode markdown-mode text-mode) . display-line-numbers-mode)
  :custom
  (tab-width 4)
  (c-basic-offset 4)
  (compilation-scroll-output t)
  (display-line-numbers-type 'relative)
  (inhibit-startup-message t)
  (split-width-threshold nil))
